package com.training.lolchampion.Service;


import com.training.lolchampion.entities.LolChampion;
import com.training.lolchampion.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolChampionServer {

    @Autowired
    LolChampionRepository lolChampionRepository;


    public List<LolChampion> findAll() {
        return this.lolChampionRepository.findAll();
    }

    public LolChampion save(LolChampion lolChampion) {
        return this.lolChampionRepository.save(lolChampion);
    }







}
