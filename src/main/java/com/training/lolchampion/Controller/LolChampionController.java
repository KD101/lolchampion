package com.training.lolchampion.Controller;

import com.training.lolchampion.Service.LolChampionServer;
import com.training.lolchampion.entities.LolChampion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping ("/api/v1/lolchampion")
public class LolChampionController {


    @Autowired
    private LolChampionServer lolChampionServer;

    @GetMapping
    public List<LolChampion> findAll() {
        return this.lolChampionServer.findAll();
    }

    public LolChampion save(@RequestBody LolChampion lolChampion) {
        return this.lolChampionServer.save(lolChampion);
    }

}
